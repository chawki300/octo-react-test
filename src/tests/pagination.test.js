import React from 'react';
import renderer from 'react-test-renderer';
import Pagination from '../components/pagination';

it('should render correctly', () => {
  const tree = renderer
    .create(<Pagination />)
    .toJSON();
  expect(tree).toMatchSnapshot();

});