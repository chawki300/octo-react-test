import React from "react";
import renderer from "react-test-renderer";
import Table from "../components/table";

const item = {
  id: 1,
  name: "Leanne Graham",
  username: "Bret",
  email: "Sincere@april.biz",
  phone: "1-770-736-8031 x56442",
};

it("should render an empty table when no items", () => {
  const elem = renderer.create(<Table />).toJSON();
  expect(elem).toMatchSnapshot();
});

it("should render a single item", () => {
  const elem = renderer.create(<Table rows={[item]} />).toJSON();
  expect(elem).toMatchSnapshot();
});

it("should render multiple item", () => {
  const items = [item, item];
  const elem = renderer.create(<Table rows={items} />).toJSON();
  expect(elem).toMatchSnapshot();
});
