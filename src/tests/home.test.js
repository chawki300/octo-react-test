import { render, screen } from '@testing-library/react';
import Home from '../routes/home';

test('renders Home', () => {
  render(<Home />);
  const linkElement = screen.getByText(/Welcome to Octo Test/i);
  expect(linkElement).toBeInTheDocument();
});
