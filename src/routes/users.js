import React, { useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import Table from "../components/table";

export default function Users() {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users.data);
  
  const getUsers = useCallback(() => {
    dispatch({ type: "GET_USERS_REQUEST" });
  }, [dispatch]);

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  return (
    <div className="center">
      <Table rows={users} />
    </div>
  );
}
