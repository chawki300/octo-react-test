import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import React, { useState } from "react";
import Users from "./users";
import Photos from "./photos";
import Home from './home';
import Drawer from "../containers/drawer";
import AppBar from "../containers/appBar";

export default function MyComponent() {
  const [isDrawerOpened, setOpenDrawer] = useState(false);

  const toggleDrawer = () => {
    setOpenDrawer(!isDrawerOpened);
  };

  return (
    <Router>
      <div className="container">
        <AppBar onClickToolbar={() => toggleDrawer()} />
        <>
          <Drawer open={isDrawerOpened} onClose={() => toggleDrawer(false)}>
            <nav>
              <ul className="links">
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/photos">Photos</Link>
                </li>
                <li>
                  <Link to="/users">Users</Link>
                </li>
              </ul>
            </nav>
          </Drawer>
        </>

        {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/photos">
            <Photos />
          </Route>
          <Route path="/users">
            <Users />
          </Route>

          <Redirect to="/home" />
        </Switch>
      </div>
    </Router>
  );
}

