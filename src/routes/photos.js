import React, { useEffect, useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Pagination from "../components/pagination";
import Card from "../components/card";

export default function Photos() {
  const dispatch = useDispatch();
  const [paginationState, setPaginationState] = useState({ start: 0, end: 10 });
  const photos = useSelector((state) => state.photos.data);

  const getPhotos = useCallback(() => {
    dispatch({ type: "GET_PHOTOS_REQUEST" });
  }, [dispatch]);

  useEffect(() => {
    getPhotos();
  }, [getPhotos]);

  return (
    <div className="photosContainer">
      <div className="cardsBox">
        {photos
          ?.slice(paginationState.start, paginationState.end)
          ?.map((item, index) => (
            <div className="cards" key={item?.id?.toString()}>
              <Card {...item} />
            </div>
          ))}
      </div>
      <div className="pagination">
        <Pagination
          count={photos?.length}
          onChange={(_event, value) =>
            setPaginationState({ start: value * 10 - 10, end: value * 10 })
          }
        />
      </div>
    </div>
  );
}
