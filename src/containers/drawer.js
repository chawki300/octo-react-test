import React, { memo, useEffect } from "react";
import PropTypes from "prop-types";
import { useLocation } from "react-router-dom";
import DrawerMui from "@mui/material/Drawer";
const Drawer = memo(({ open = false, onClose = () => {}, children = null }) => {
  let location = useLocation();

  useEffect(() => {
    if (open) {
      onClose();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  return (
    <DrawerMui anchor={"left"} open={open} onClose={() => onClose()}>
      {children}
    </DrawerMui>
  );
});

Drawer.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.node,
};
export default Drawer;
