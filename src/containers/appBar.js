import React, { memo } from "react";
import { useLocation } from "react-router-dom";
import PropTypes from "prop-types";
import AppBarMui from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import _capitalize from "lodash/capitalize";
const AppBar = memo(({ onClickToolbar = () => {} }) => {
  let { pathname } = useLocation();
  return (
    <AppBarMui position="static">
      <Toolbar onClick={() => onClickToolbar()}>
        <IconButton
          size="large"
          edge="start"
          color="inherit"
          aria-label="menu"
          sx={{ mr: 2 }}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          {_capitalize(pathname.replace("/", ""))}
        </Typography>
      </Toolbar>
    </AppBarMui>
  );
});

AppBar.propTypes = {
  onClickToolbar: PropTypes.func,
};
export default AppBar;
