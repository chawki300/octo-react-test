
import React from "react";

import Card from "./card";

export default {
  component: Card,
  title: "Card",
};

const Template = (args) => <Card {...args} />;

export const Default = Template.bind({});
Default.args = {
  albumId: 1,
  id: 1,
  title: "accusamus beatae ad facilis cum similique qui sunt",
  url: "https://via.placeholder.com/600/92c952",
  thumbnailUrl: "https://via.placeholder.com/150/92c952",
};
