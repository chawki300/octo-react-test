import * as React from "react";
import PaginationMui from "@mui/material/Pagination";
import PropTypes from "prop-types";

export default function Pagination({ count = 10, onChange = () => {} }) {
  return (
    <PaginationMui
      count={count / 10}
      color="secondary"
      showFirstButton
      showLastButton
      onChange={(event, value) => onChange(event, value)}
    />
  );
}
Pagination.propTypes = {
  count: PropTypes.number,
  onChange: PropTypes.func,
};
