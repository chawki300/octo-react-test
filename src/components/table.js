import React, { memo } from "react";
import { DataGrid } from "@mui/x-data-grid";
import PropTypes from "prop-types";

const defaultColumns = [
  { field: "id", headerName: "ID", width: 50 },
  { field: "name", headerName: "Nom", width: 250 },
  { field: "email", headerName: "Email", width: 250 },
  { field: "username", headerName: "Identifiant", width: 250 },
  {
    field: "phone",
    headerName: "Téléphone",
    width: 250,
  },
];

const DataTable = memo(({ columns = defaultColumns, rows = [] }) => {
  return (
    <div style={{ height: "75%", width: "80%" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[10]}
      />
    </div>
  );
});
DataTable.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.node,
};

export default DataTable;
