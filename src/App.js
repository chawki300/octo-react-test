import React, { Suspense } from "react";
import { Provider } from "react-redux";
import store from "./redux/store";
import RoutesComponent from "./routes";
// global styles
import "./App.css";

export default function App() {
  return (
    <Provider store={store()}>
      <Suspense fallback="loading">
        <RoutesComponent />
      </Suspense>
    </Provider>
  );
}
