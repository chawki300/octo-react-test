import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import {persistReducer} from "redux-persist";
import createSagaMiddleware from 'redux-saga';
import {createLogger} from 'redux-logger';
import persistConfig from '../config/persist';

const logger = createLogger({
  diff: true,
  duration: true,
});

// creates the store
const configureStore =  (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */
  const persistedReducer = persistReducer(persistConfig, rootReducer);

  const middleware = [logger];
  const enhancers = [];

  /* ------------- Navigation Middleware ------------ */
  //middleware.push(appNavigatorMiddleware);

  /* ------------- Saga Middleware ------------- */

  const sagaMiddleware = createSagaMiddleware();
  middleware.push(sagaMiddleware);

  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware));

  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron

  const store = createStore(
    persistedReducer,
    composeWithDevTools(...enhancers),
  );



  // kick off root saga
  let sagasManager = sagaMiddleware.run(rootSaga);

  return {
    store,
    sagasManager,
    sagaMiddleware,
  };
};

export default configureStore

