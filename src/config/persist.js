import immutablePersistenceTransform from "./immutablePersistenceTransform";
import storage from "redux-persist/lib/storage";

const REDUX_PERSIST = {
  active: true,
  reducerVersion: "1.0",
  key: "primary",
  storage: storage,
  // Reducer keys that you do NOT want stored to persistence here.
  blacklist: ["users"],
  // Optionally, just specify the keys you DO want stored to persistence.
  whitelist: [],
  transforms: [immutablePersistenceTransform],
};

export default REDUX_PERSIST;
