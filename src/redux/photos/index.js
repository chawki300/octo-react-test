import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  getPhotosRequest: ['params'],
  getPhotosSuccess: ['response'],
  getPhotosFailure: ['error'],
  resetError: [],
});

export const GetPhotosTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: [],
  hasError: false,
  fetching: false,
  message:""
});

/* ------------- Selectors ------------- */

/* ------------- Reducers ------------- */

export const getPhotosRequest = (state) => {
  return state.merge({
    ...state,
    hasError: false,
    message: null,
    fetching: true,
  });
};

export const getPhotosSuccess = (state, {response}) => {
  return state.merge({
    fetching: false,
    hasError: false,
    data: response,
  });
};

// failed to Request
export const getPhotosFailure = (state, {error}) => {
  return state.merge({
    ...state,
    message: error,
    hasError: true,
    fetching: false,
  });
};

export const resetError = state => {
  return state.merge({
    ...state,
    message: null,
    hasError: false,
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  /* get photos */
  [Types.GET_PHOTOS_REQUEST]: getPhotosRequest,
  [Types.GET_PHOTOS_SUCCESS]: getPhotosSuccess,
  [Types.GET_PHOTOS_FAILURE]: getPhotosFailure,
  [Types.RESET_ERROR]: resetError,
});
