import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import configureStore from "../config/configureStore";
import ReduxPersist from "../config/persist";
import rootSaga from "../sagas";
/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  users: require('./users').reducer,
  photos: require('./photos').reducer,
});

export const rootReducer = (state, action) => {
  return reducers(state, action);
};

const store = () => {
  let finalReducers = rootReducer;
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist;
    finalReducers = persistReducer(persistConfig, rootReducer);
  }
  let { store /*sagasManager, sagaMiddleware*/ } = configureStore(
    finalReducers,
    rootSaga
  );
  return store;
};
export default store;
