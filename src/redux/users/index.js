import {createReducer, createActions} from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  getUsersRequest: ['params'],
  getUsersSuccess: ['response'],
  getUsersFailure: ['error'],
  resetError: [],
});

export const GetUsersTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: [],
  hasError: false,
  fetching: false,
  message:""
});

/* ------------- Selectors ------------- */

/* ------------- Reducers ------------- */

export const getUsersRequest = (state) => {
  return state.merge({
    ...state,
    hasError: false,
    message: null,
    fetching: true,
  });
};

export const getUsersSuccess = (state, {response}) => {
  return state.merge({
    fetching: false,
    hasError: false,
    data: response,
  });
};

// failed to Request
export const getUsersFailure = (state, {error}) => {
  return state.merge({
    ...state,
    message: error,
    hasError: true,
    fetching: false,
  });
};

export const resetError = state => {
  return state.merge({
    ...state,
    message: null,
    hasError: false,
  });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  /* get users */
  [Types.GET_USERS_REQUEST]: getUsersRequest,
  [Types.GET_USERS_SUCCESS]: getUsersSuccess,
  [Types.GET_USERS_FAILURE]: getUsersFailure,
  [Types.RESET_ERROR]: resetError,
});
