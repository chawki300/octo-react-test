import { call } from "redux-saga/effects";
import errorsType from "./errorTypes";
import _get from "lodash/get";
/**
 * Wrap api call
 */
export default function* wrapApiCall(fn, ...args) {
  while (true) {
    // format response object
    let res = {
      success: false,
      data: null,
      error: null,
      status: null,
    };

    const response = yield call(fn, ...args);
    const { ok, data, problem } = response;
    if (ok || data.length > 0) {
      res.success = true;
      res.data = data;
    } else {
      res.success = false;
      res.data = null;
      if (data && "status" in data) {
        res.error = data.status.message;
      } else {
        res.error = errorsType[problem];
      }
    }
    res.status = _get(response, "status", null);
    return res;
  }
}
