const errors = {
  CLIENT_ERROR: "SERVER not available",
  SERVER_ERROR: "server error please contact support",
  TIMEOUT_ERROR: "Server didn't respond in time",
  CONNECTION_ERROR: "Connection error",
  NETWORK_ERROR: "Netowrk Error",
};
export default errors;
