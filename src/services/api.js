// a library to wrap and simplify api calls
import axios from "axios";

const baseURL = "https://jsonplaceholder.typicode.com/";

export let api = axios.create({
  // base URL is read from the "constructor"
  baseURL,
  // here are some default headers
  headers: {
    "Cache-Control": "no-cache",
    "Content-Type": "application/json",
  },
  // 10 second timeout...
  timeout: 10000,
});

const create = () => {

  const getPhotos = (params) => {
    return api.get("/photos", params);
  };

  const getUsers = (params) => {
    return api.get("/users", params);
  };

  return {
    getPhotos, 
    getUsers 
  };
};


// let's return back our create method as the default.
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  create,
};
