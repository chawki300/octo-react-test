import { call, put } from "redux-saga/effects";
import Actions from "../../redux/users";
import wrapApiCall from "../../services/wrapApiCall";
export function* getUsersSaga(axios, { params }) {
  const { getUsers } = axios;
  const { success, data, error } = yield call(wrapApiCall, getUsers, params);
  if (success) {
    yield put(Actions.getUsersSuccess(data));
  } else {
    yield put(Actions.getUsersFailure(error));
  }
}
