import { call, put } from "redux-saga/effects";
import Actions from "../../redux/photos";
import wrapApiCall from "../../services/wrapApiCall";
export function* getPhotosSaga(axios, { params }) {
  const { getPhotos } = axios;
  const { success, data, error } = yield call(wrapApiCall, getPhotos, params);
  if (success) {
    yield put(Actions.getPhotosSuccess(data));
  } else {
    yield put(Actions.getPhotosFailure(error));
  }
}
