import { takeLatest, all } from "redux-saga/effects";
import { GetUsersTypes } from "../redux/users";
import { getUsersSaga } from "./users";

import { GetPhotosTypes } from "../redux/photos";
import { getPhotosSaga } from "./photos";

import API from "../services/api";

export const api = API.create();

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.

/* ------------- Connect Types To Sagas ------------- */

export default function* root() {
  yield all([
    // some sagas only receive an action
    takeLatest(GetUsersTypes.GET_USERS_REQUEST, getUsersSaga, api),
    takeLatest(GetPhotosTypes.GET_PHOTOS_REQUEST, getPhotosSaga, api),

  ]);
}
